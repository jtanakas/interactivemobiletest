﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public static CameraFollow Instance;

	void Awake () {
		if (Instance == null) {
			Instance = this;
		}
	}

	public Transform objectToFollow;

	public float minFoV = 13f;
	public float maxFoV = 65f;
	public float minDist = 45f;
	public float maxDist = 10f;

	public float mapSize = 200f;


	public Texture mapTexture;

	public Camera cam;
	public float camExtraDistance = 0f;

	// Use this for initialization
	void Start () {
		cam = Camera.main;
		mapSize = mapSize * Screen.height / 1080f; 
	}

	void OnGUI () {
		if (mapTexture != null) {
			GUI.DrawTexture (new Rect (Screen.width - mapSize + 1f, Screen.height - mapSize, mapSize, mapSize), mapTexture);
		}
	}

	//12 fov - 45 unit
	//65 fov - 10 unit

	public void SetFocus () {
		if (WarriorController.controlledRigidbody == null)
			return;
		objectToFollow = WarriorController.controlledRigidbody.transform;
		if (objectToFollow != null && cam != null) {
			camExtraDistance += Input.GetAxis ("Mouse ScrollWheel");
			transform.LookAt (objectToFollow);
			float dist = (objectToFollow.position - transform.position).magnitude;

			cam.fieldOfView = Mathf.Clamp ( (maxFoV - minFoV) / (maxDist - minDist) * (camExtraDistance + dist) + ((maxDist - minDist) * maxFoV - maxDist * (maxFoV - minFoV)) / (maxDist - minDist), 5f, 100f);
		} else if (objectToFollow == null) {
			Debug.LogWarning ("You should set an object to follow first.");
		} else if (cam == null) {
			Debug.LogWarning ("You screen should have at least one camera set with MainCamera tag");
		}
	}

	// Update is called once per frame
	void Update () {
		SetFocus ();
		
	}
}
