﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;

	public GameObject firstObject;

	public static bool rotateWhileRunning = true;
	public static bool showDebugInfo = true;

	#if UNITY_EDITOR
	[SerializeField]
	#endif
	private GameObject _player;
	public GameObject player {
		get {
			return (_player == null ? _player = GameObject.FindGameObjectWithTag ("Player") : _player );
		}
	}
	[SerializeField]
	private SwordmanController _playerController;
	public SwordmanController playerController {
		get {
			return (_playerController == null ? _playerController = player.GetComponent<SwordmanController>() : _playerController);
		}
	}

	void Awake () {
		if (Instance == null) {
			Instance = this;

		}
	}

	// Use this for initialization
	void Start () {
		Application.runInBackground = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			StartGame ();
		}
		if (Input.GetKeyDown (KeyCode.Q)) {
			rotateWhileRunning = !rotateWhileRunning;
		}
		if (Input.GetKeyDown (KeyCode.U)) {
			Time.timeScale = 1f;
		}
		if (Input.GetKeyDown (KeyCode.I)) {
			Time.timeScale += 1f;
		}
		if (Input.GetKeyDown (KeyCode.O)) {
			Time.timeScale = .4f;
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			Time.timeScale = .0f;
		}
	}

	void StartGame () {
		print ("Vai la cara");
		playerController.SetTarget (firstObject.transform);
		playerController.WalkToTarget (null);
	}

	public void GameOver () {
		
	}
}
