﻿using UnityEngine;
using System.Collections;

public class CollectableCube : MonoBehaviour {

	public GameObject nextObject;
	public float attackForceBonus = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col) {
		if (col.tag == "Player") {
			Collect (col);

		}
	}

	public virtual void Collect (Collider col) {
		StartCoroutine (RiseUp ());
		col.GetComponent<WarriorController> ().AddAttackForce (attackForceBonus);
		if (nextObject != null) {
			col.GetComponent<WarriorController> ().SetTarget (nextObject.transform);
		}
	}

	IEnumerator RiseUp () {
		Vector3 startPos = transform.position;
		float elapsedTime = 0f;
		float duration = 1f;
		Color startColor = GetComponent<Renderer> ().material.color;
		while (elapsedTime < duration) {
			transform.position = Vector3.Lerp (startPos, startPos + Vector3.up * 3.5f, (elapsedTime / duration));
			transform.localScale = Vector3.Lerp (Vector3.one, Vector3.one * 2f, (elapsedTime / duration));
			GetComponent<Renderer> ().material.color = Color.Lerp (startColor, Color.clear, (elapsedTime / duration));
			elapsedTime += Time.fixedDeltaTime;
			yield return null;
		}
		// round values just in case
		transform.position = startPos + Vector3.up * 3.5f;
		transform.localScale = Vector3.one * 2f;
		GetComponent<Renderer> ().material.color = Color.clear;
		Destroy (gameObject);
	}
}
