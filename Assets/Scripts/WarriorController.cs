﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(WarriorController), true)]
public class WarriorControllerEditor : Editor {
	public override void OnInspectorGUI () {
		base.OnInspectorGUI ();

		WarriorController warrior = target as WarriorController;

		EditorStyles.label.wordWrap = true;
		EditorStyles.label.richText = true;
		if (WarriorController.controlledRigidbody != null && !WarriorController.controlledRigidbody.Equals(warrior.m_rigidbody)) {
			EditorGUILayout.LabelField("Controlled Unit (Test Only)", EditorStyles.boldLabel); 
			if (GUILayout.Button ("Set as Controlled")) {
				Debug.Log ("Setando: " + warrior.gameObject.name);
				WarriorController.controlledRigidbody = warrior.m_rigidbody;
			}
		}
	}
}
#endif

[RequireComponent(typeof(Rigidbody))]
public class WarriorController : MonoBehaviour {
	public Rigidbody m_rigidbody {
		get {
			return GetComponent<Rigidbody> ();
		}
	}
	public Animator m_anim {
		get {
			return GetComponent<Animator> ();
		}
	}
	public Vector3 centerPosition {
		get {
			return transform.position + Vector3.up * 1.5f;
		}
	}


	[Header("Warrior Attributes")]
	public float startAttackForce = 0f;
	public float startEnergy = 0f;
	public float speedMultiplier = 5f;

	[Header("Secondary Attributes")]
	public bool cameraStart = false;
	public static Rigidbody controlledRigidbody;
	public float findRange = 1f;
	public GameObject nextTarget;
	protected bool isAttacking = false;
	protected bool isDead = false;
	protected int enemiesKilled = 0;

	[Range(0f, 1f)]
	public float attackDamageTime = 0.366f;

	public Text uiDebugText;

	public float searchRange {
		get {
			return findRange * 2f;
		}
	}

	#if UNITY_EDITOR
	[Header("Checking Values")]
	[SerializeField]
	#endif
	protected float attackForce = 0f;
	#if UNITY_EDITOR
	[SerializeField]
	#endif
	protected float energy = 0f;

	public Transform mainTarget;
	private float rotationSpeed = 1f;

	public void OnDrawGizmos () {
		Gizmos.DrawWireSphere (centerPosition, searchRange);
	}

	public string enemyTag {
		get {
			return (tag == "Player" ? "Enemy" : "Player");
		}
	}

	// Use this for initialization
	public virtual void Start () {
		uiDebugText = GetComponentInChildren<Text> ();
		if (cameraStart) {
			controlledRigidbody = m_rigidbody;
		}
		m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
		AddEnergy (startEnergy);
		AddAttackForce (startAttackForce);
	}
	
	// Update is called once per frame
	public virtual void Update () {

		if (uiDebugText != null && GameManager.showDebugInfo) {
			uiDebugText.text = "Attack Force: " + attackForce + "\n" +
			"Energy: " + energy + "\n";
		}

		if (controlledRigidbody != null && controlledRigidbody.name.Equals(this.name)) {
			//controlledRigidbody.velocity = ;
			Walk (Input.GetAxis ("Vertical"));
			Rotate (new Vector3 (0f, Input.GetAxis ("Horizontal"), 0f));
		}

		Collider[] founds = Physics.OverlapSphere (centerPosition, searchRange);
		foreach (Collider found in founds) {
			if (found.tag == enemyTag) {
				EnemyFound (found.GetComponent<WarriorController> ());
			}
		}

		if (GameManager.rotateWhileRunning && mainTarget != null) {
			////print ("Rodando");
			// rotating body toward
			// source: http://docs.unity3d.com/ScriptReference/Vector3.RotateTowards.html
			Vector3 targetDir = PositionOnFloor(mainTarget.position) - transform.position;

			float step = rotationSpeed * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
			transform.rotation = Quaternion.LookRotation (newDir);
		}
	}

	// used to compare as if the object is on the floor avoiding the player to face up.
	private Vector3 PositionOnFloor (Vector3 position) {
		return new Vector3 (position.x, 0f, position.z);
	}

	public enum Side
	{
		Ahead,
		Left,
		Right
	}

	public Side RelativePos (Transform objectTransform) {
		var relativePoint = transform.InverseTransformPoint(objectTransform.position);
		if (relativePoint.x < 0.0) {
			return Side.Left;
		} else if (relativePoint.x > 0.0) {
			return Side.Right;
		} else {
			return Side.Ahead;
		}
		
	}

	public bool enemyKilled = false;

	public virtual void EnemyFound (WarriorController warrior) {
		////print (this.name + " - " + enemyTag + " Found: " + warrior.name);

		var cubeDir = transform.position - warrior.transform.position;
		var angle = Vector3.Angle(cubeDir, warrior.transform.forward);
		float dot = Vector3.Dot(transform.forward, (warrior.transform.position - transform.position).normalized);
		float ang = Mathf.Atan (dot);

		//TODO: check direction first

		if (!IsAlmostFacing (warrior.transform)) {
			Vector3 targetDir = PositionOnFloor(warrior.transform.position) - transform.position;
			float step = rotationSpeed * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 10.0F);
			transform.rotation = Quaternion.LookRotation (newDir);
		} else {
			AnimatorStateInfo info = m_anim.GetCurrentAnimatorStateInfo (0);
			AnimatorStateInfo infoNext = m_anim.GetNextAnimatorStateInfo (0);

			if (!warrior.isDead && !this.isAttacking && !this.isDead) {
				StopAllCoroutines ();
				print (this.name + " attacks " + warrior.name + " Angle: " + angle + " : " + ang + " Dot: " + dot + " Position: " + RelativePos (warrior.transform));
				isAttacking = true;
				if (warrior.tag == enemyTag) {
					// stop walking
					StopAllCoroutines ();
					m_anim.SetTrigger ("Attack");
					StartCoroutine (warrior.ApplyDamageAfter (attackForce, info.length * attackDamageTime, (enemyKilledIn) => {
						enemyKilled = enemyKilledIn;
					}));
				
					StartCoroutine (warrior.ResetAttackAfter (info.length));
					// check if enemy was killed after a while or after enemy damages me
					StartCoroutine (CheckEnemyKilled (warrior, 2f));
				}
			}
		}
	}

	IEnumerator CheckEnemyKilled (WarriorController warrior, float waitTime) {
		//print ("INCIOU : CheckEnemyKilled");
		
		yield return new WaitForSeconds (waitTime);

		if(enemyKilled) { 
			this.energy = this.startEnergy;
			this.attackForce = 0f;
			// resets player attack force after killing an enemy
			if (tag == "Player") {
				enemiesKilled++;
				if (enemiesKilled < 3) {
					SetTarget (warrior.nextTarget.transform);
					Invoke ("WalkToTarget", 1.5f);
				} else {
					//print ("VENCEU O JOGO!");
				}
			}
		}
		
	}

	IEnumerator ResetAttackAfter (float time) {
		yield return new WaitForSeconds (time);
		isAttacking = false;
		
	}

	IEnumerator ApplyDamageAfter (float damage, float time, System.Action<bool> enemyKilled) {
		//print ("INCIOU : ApplyDamageAfter");
		
		yield return new WaitForSeconds (time);
		// only damage if not dead yet
		//print (name + " is dead : " + isDead);
		if (!isDead) {
			if (Damage (damage)) {
				//print (name + " deu dano : " + damage);
				enemyKilled (true);
			}
		}
		
	}

	public bool Damage (float damage) {
		if (damage >= energy) {
			energy = 0;
			Die ();
			return true;
		} else {
			energy -= damage;
			return false;
		}
	}

	public void Die () {
		//print (this.name + " dead!");
		isDead = true;

		m_anim.SetTrigger ("Die");
		Invoke ("DestroyCollider", m_anim.GetCurrentAnimatorStateInfo (0).length + 1.5f);

		if (tag == "Player") {
			GameManager.Instance.GameOver ();
		}
	}

	public void DestroyCollider () {
		GetComponent<Collider> ().enabled = false;
		Invoke ("DestroyBody", 1f);
	}

	public void DestroyBody () {
		Destroy (gameObject);
	}

	public void Rotate (Vector3 amount) {
		//transform.Rotate (amount);
	}

	public void Walk (float speed) {
		m_rigidbody.velocity = transform.forward * speed * speedMultiplier;
		m_anim.SetFloat ("Speed", speed);
	}

	public void AddAttackForce (float amount) {
		attackForce += amount;
	}

	public void AddEnergy (float amount) {
		energy += amount;
	}

	public void SetTarget (Transform target) {
		this.mainTarget = target;
	}

	public void RotateToNextTarget (System.Action callback = null) {
		StartCoroutine (RotateToDirection (mainTarget, callback));
	}

	public void WalkToTarget (System.Action callback = null) {
		
		StartCoroutine (WalkToPosition (mainTarget.position, callback));
	}
	public void WalkToTarget () {
		StartCoroutine (WalkToPosition (mainTarget.position, null));
	}

	IEnumerator WalkToPosition (Vector3 position, System.Action callback) {
		//print (name + "INCIOU : WalkToPosition");
		
		float maxDistance = (mainTarget.tag == enemyTag ? 1f : 0.6f);
		float distance = (PositionOnFloor(position) - transform.position).magnitude;
		while (distance > maxDistance) {
			distance = (PositionOnFloor(position) - transform.position).magnitude;
			////print (distance);
			Walk (1f);
			yield return null;
		}
		Walk (0f);
		//yield return new WaitForSeconds (2f);
		// stops walking

		if (callback != null) {
			callback ();
		} else {
			RotateToNextTarget ();
		}

	}

	public bool IsAlmostFacing (Transform targetT) {
		if (targetT != null) {
			float dot = Vector3.Dot (transform.forward, (targetT.position - transform.position).normalized);
			return dot > 0.98f;
		}
		return false;

	}

	IEnumerator RotateToDirection (Transform targetT, System.Action callback = null) {
		if (targetT == null)
			yield break;
		float maxDistance = (mainTarget.tag == enemyTag ? searchRange : 0.1f);
		while (targetT != null && !IsAlmostFacing(targetT)) { 
			if (targetT == null)
				yield break;
			Vector3 targetDir = PositionOnFloor(targetT.position) - transform.position;
			float step = rotationSpeed * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 10.0F);
			transform.rotation = Quaternion.LookRotation (newDir);

			yield return null;
		}
		if (callback != null) {
			callback ();
		} else {
			WalkToTarget (null);
		}
		
	}
}
