﻿using UnityEngine;
using System.Collections;

public class PurpleCube : CollectableCube {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static int collecteds = 0;
	public GameObject greenCube;
	public GameObject spearman;

	public override void Collect (Collider col)
	{
		base.Collect (col);
		collecteds++;
		print ("Coleatdos: " + collecteds);
		if (collecteds == 5) {
			greenCube = Instantiate(Resources.Load<GameObject> ("GreenCube"));
			spearman = transform.parent.Find ("Spearman").gameObject;

			greenCube.transform.position = spearman.transform.position - spearman.transform.forward * 4f;

			greenCube.GetComponent<CollectableCube> ().nextObject = nextObject;
			nextObject = greenCube;
			col.GetComponent<WarriorController> ().SetTarget (nextObject.transform);
		}


	} 
}
