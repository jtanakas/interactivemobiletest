﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpearmanController : WarriorController {

	public List<GameObject> points;
	private int currentPoint = 0;
	private int lastSpot = 1;
	public Transform nextSpot {
		get {
			lastSpot = currentPoint;
			if (points.Count == 0) {
				return null;
			}
			do {
				currentPoint = Random.Range(0, points.Count);
				if (currentPoint >= points.Count) {
					currentPoint = 0;
				}
				if (points.Count > 0 && points [currentPoint] == null)  {
					points.RemoveAt(currentPoint);
				}
			} while (currentPoint == lastSpot || points [currentPoint] == null);
			return points [currentPoint].transform;

		}
	}

	public override void Start ()
	{
		base.Start ();
		Patrol ();
	}



	public void Patrol () {
		Transform next = nextSpot;
		if (next != null) {
			SetTarget (next);
			RotateToNextTarget (StartWalk);
		}
	}

	public void StartWalk () {
		WalkToTarget (Patrol);
	}


	public override void Update ()
	{
		base.Update ();
	}






}
