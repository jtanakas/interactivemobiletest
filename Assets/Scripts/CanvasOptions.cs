﻿using UnityEngine;
using System.Collections;

public class CanvasOptions : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.SetActive (GameManager.showDebugInfo);
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (Camera.main.transform);
	}
}
